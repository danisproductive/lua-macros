# lua-macros

My take on Lua Macros\
A script, written in lua, allowing you to designate any device with an HID (A keyboard for example)\
to have a custom action for any of it`s buttons.

## Running the script
Download the LuaMacros executable from here [luamacros.zip](http://www.hidmacros.eu/luamacros.zip)\
To run it on start up you can use the LuaMacros-start.bat script \
or run LuaMacros [script-path]/macros.lua in the cmd

### My changes to the original script
* Reduce the code by making a generic callback function for all keys, taking the actions from a config map.
* Adds the abillity to use a modifer key which allows each button to have an additional macro.
* Made the application minimize and go to the tray once it runs so it doesn't bother if you set it to run on startup

### Original script
Original script can be found [here](https://gist.github.com/GalaxyBrainHuman/79d4d870e7473fd7e2ef892609bf8200)
